
#include "stm32f0xx_hal.h"
#include "wm_bus.h"
#include "s2lp.h"

/*******************************************************************************
 Definicje zmiennych
*******************************************************************************/

extern volatile uint8_t FLAGS0;

volatile uint16_t WMBUSS_IrqBf;
volatile uint8_t WMB_pnt =0;
volatile uint8_t WMB_buf[300] __attribute__((aligned(4)));
volatile uint8_t WMB_Idbuf[300] __attribute__((aligned(4)));
volatile uint8_t Il_wmb =0;
volatile uint8_t Stan_wmb =0;
volatile uint8_t Cnt_bf =0;
volatile uint8_t WMBUSS_Btcnt =0;
volatile uint8_t Rssi =0;
volatile uint8_t Il_BtRx =0;

volatile uint8_t Tab3of6[64] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x03,0xFF,0x01,0x02,0xFF,
																0xFF,0xFF,0xFF,0x07,0xFF,0xFF,0x00,0xFF,0xFF,0x05,0x06,0xFF,0x04,0xFF,0xFF,0xFF,
																0xFF,0xFF,0xFF,0x0B,0xFF,0x09,0x0A,0xFF,0xFF,0x0F,0xFF,0xFF,0x08,0xFF,0xFF,0xFF,
																0xFF,0x0D,0x0E,0xFF,0x0C,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
												
/*******************************************************************************
 Odbior danych WM_BUSS
*******************************************************************************/
																
void Rx_wmbuss(void)
 {
	uint16_t i=0;

	WMBUSS_IrqBf <<= 1;  	 
	 
	if(DATA_S2_GPIO_Port ->IDR & DATA_S2_Pin)
	 {
		WMBUSS_IrqBf++;
	 }	 	
	WMBUSS_Btcnt++;	

/*******************************************************************************/	 	 
	 
	if(Stan_wmb == 0)
	 {			 
	  if((WMBUSS_IrqBf & 0x0FFF) == 0x043D)
	   {
			WMBUSS_IrqBf =0;   		  
			WMBUSS_Btcnt =0;   
			Stan_wmb =1;			
		 }
    return;  
	 }	 
		 
/*******************************************************************************/	 
	 
 if(Stan_wmb == 1)		 
  {  
	 if(WMBUSS_Btcnt == 12)	 
	  {  
	   WMBUSS_Btcnt = 0;
					
		 if(Tab3of6[WMBUSS_IrqBf & 0x003F] < 16)
		  {  
			 WMB_buf[0] = Tab3of6[WMBUSS_IrqBf & 0x003F]; 	 
		  }	
		 else
		  { 
			 WMBUSS_IrqBf =0;   		  				
			 Stan_wmb =0;
			 return;
			} 		
			
		 if(Tab3of6[(WMBUSS_IrqBf >> 6) & 0x003F] < 16)
		  {  
			 WMB_buf[0] |= Tab3of6[(WMBUSS_IrqBf >> 6) & 0x003F] << 4; 	 
		  }	
		 else
		  { 
			 WMBUSS_IrqBf =0;   		  
			 Stan_wmb =0;
			 return;
			}
			
		 if(WMB_buf[0] > 9)
		  { 		 	
		   Cnt_bf = WMB_buf[0] - 9;
		   WMBUSS_IrqBf =0;   		  
		   WMB_pnt =1;	
		   Il_BtRx =9;	
  	   Stan_wmb =2;							 					 
			}	
		 else
		  { 	
			 WMBUSS_IrqBf =0;   		  
			 Stan_wmb =0;
		  }						
		}
	 return;		
  }		
 
/*******************************************************************************/	 		
		
 if(Stan_wmb == 2)		 
  {  
	 if(WMBUSS_Btcnt == 12)	 
	  {  
	   WMBUSS_Btcnt = 0;
					
		 if(Tab3of6[WMBUSS_IrqBf & 0x003F] < 16)
		  {  
			 WMB_buf[WMB_pnt] = Tab3of6[WMBUSS_IrqBf & 0x003F]; 	 
		  }	
		 else
		  { 
			 WMBUSS_IrqBf =0;   		  
			 Stan_wmb =0;
			 return;
			} 		
			
		 if(Tab3of6[(WMBUSS_IrqBf >> 6) & 0x003F] < 16)
		  {  
			 WMB_buf[WMB_pnt] |= Tab3of6[(WMBUSS_IrqBf >> 6) & 0x003F] << 4; 	 
			 Il_BtRx--;					
		  }	
		 else
		  { 
			 WMBUSS_IrqBf =0;   		  
			 Stan_wmb =0;
			 return;
			}
						
		 if(Il_BtRx == 0)
		  {
			 Il_BtRx =2;					
			 Stan_wmb =3;					
		  }			 
		 else
		  {	
			 WMB_pnt++;
			}
		}		
	 return;	
	}		
 
/*******************************************************************************/	 					
			
	if(Stan_wmb == 3)		 
   {  
	  if(WMBUSS_Btcnt == 12)	 
	   {  
	    WMBUSS_Btcnt = 0;
					
		  if(Tab3of6[WMBUSS_IrqBf & 0x003F] > 15)
		   {  			  
			  WMBUSS_IrqBf =0;   		  
			  Stan_wmb =0;
			  return;
			 } 		
			
		 if(Tab3of6[(WMBUSS_IrqBf >> 6) & 0x003F] > 15)
		  {  
			 WMBUSS_IrqBf =0;   		  
			 Stan_wmb =0;
			 return;	
		  }	
		 else
		  {				 				
			 Il_BtRx--;					
		  }	
		 					
		 if(Il_BtRx == 0)
		  {			
			 if(Cnt_bf == 0)
			  {		
				 WMBUSS_IrqBf =0;   		  
				 Stan_wmb =0;	
					
				 if((FLAGS0 & Dt_wmb) == 0x00)
			    { 
			     Read_Spr(0xEF);	
		       WMB_Idbuf[0]	 = 146 - Read_Spr(0xEF);
           Rssi = WMB_Idbuf[0];												   											
			     Il_wmb = WMB_buf[0] + 2;								 
			     for(i=0; i<255; i++)
				    {	
				     WMB_Idbuf[i+1] = WMB_buf[i]; 
				    }			 							
			     FLAGS0 |= Dt_wmb;				 						 													     
					}
				 return;		
				}	
						 		
			 if(Cnt_bf  >= 16)
			  {		
				 Il_BtRx = 16;
				 Cnt_bf -= 16;
				 WMBUSS_IrqBf =0;   		  		     
  	     Stan_wmb =2;							
				 WMB_pnt++;				
				 return;
				} 			
								
			 if(Cnt_bf  < 16)
			  {		
				 Il_BtRx = Cnt_bf;
				 Cnt_bf =0;
				 WMBUSS_IrqBf =0;   		  		     
  	     Stan_wmb =2;							
				 WMB_pnt++;				
				 return;
				} 		
			}		
		}		 	
	 return;	
	}					
}		  																
																
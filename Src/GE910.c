#include "stm32f0xx_hal.h"
#include "ge910.h"
#include "usart.h"
#include "i2c.h"

/*******************************************************************************
 Definicje zmiennych
*******************************************************************************/

volatile uint8_t Stan_ge =0;
volatile uint16_t Tim_ge =0;
uint8_t BufIrqGE[1100] __attribute__((aligned(4)));
uint8_t BufIdlGE[1100] __attribute__((aligned(4)));
uint8_t BufIdlGETmp[550] __attribute__((aligned(4)));
uint16_t Ind_RxIdlGE =0;
uint16_t Ind_RxIrqGE =0;	  	 
uint8_t BufTxGsm[300] __attribute__((aligned(4)));
uint8_t Buf_imei[32] __attribute__((aligned(4)));
uint8_t Cnt_imei =255;
uint8_t TmpI2C[40];
volatile uint8_t Ctn_errKnt =0;
volatile uint8_t Atc_srv =0xFF;
volatile uint8_t IP_gprs[4] __attribute__((aligned(4)));
volatile uint16_t Port_gprs =0; 
volatile uint8_t ByteToAscStr[5] __attribute__((aligned(4)));
volatile uint8_t Cnt_errC =0;
volatile uint16_t U_TxCnt =0;
volatile uint16_t U_Txpnt =0;
volatile uint16_t Tim_csq =0; 
volatile uint8_t Rssi_gsm =0;
volatile uint16_t Il_srv =0;
volatile uint8_t Cnt_srv =0;

extern volatile uint8_t FLAGS0; 
extern volatile uint8_t FLAGS1; 
extern uint8_t Buf_i2c[260] __attribute__((aligned(4)));
extern volatile uint16_t Il_gprs;
extern uint8_t Buf_gprs[300] __attribute__((aligned(4)));
extern volatile uint8_t Nr_srv;	
extern volatile uint32_t Mask_ld2;
extern volatile uint16_t Tim_ld3;

/*******************************************************************************
 Obsluga telefonu GE910
*******************************************************************************/

void GE910_FN(void)
 {
	uint16_t i,j=0;	 	
	 
	if(Stan_ge < 0x18)
	 {	
    Mask_ld2 =0;
	 }	 
	 
	if(Test_dtgsm(Tab_srg)) 					// gdy sa dane odebrane z serwera		
	 {
	  Rx_stSrv();
	 }		
		 
	if(Test_dtgsm(Tab_NoCr))					// gdy nastapilo rozlaczenie			
	 {	
	  Atc_srv =0xFF;
	 }	

	if(Test_dtgsm(Tab_RxCSQ))			   // gdy sa dane o poziomie sygnalu
	 {	
	  Obs_csq();
	 }	  
	 	 
	if(Test_dtgsm(Tab_RxCSQ))			   // gdy sa dane o poziomie sygnalu
	 {	
	  Obs_csq();
	 }	 

  switch(Stan_ge)
	 {	
/******************************************************************************/	  
		 
		case 0x00:	 												// wylaczenie telefonu
		if(Tim_ge == 0)
		 {					
		  HAL_GPIO_WritePin(MON_GPIO_Port, MON_Pin, GPIO_PIN_RESET); 
		  HAL_UART_MspDeInit(&huart1);
		  Tim_ge = 5000;
		  Stan_ge++;			
		 }	 
		break;		
		
/******************************************************************************/	  		
		
		case 0x01:	 														
		if(Tim_ge == 0)
		 {				
		  HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(RES_CE_GPIO_Port, RES_CE_Pin, GPIO_PIN_SET);						
		  Tim_ge = 5000;
		  Stan_ge++;			
		 }	 
		break;		
		
/******************************************************************************/	  				
		
		case 0x02:	 												// start zasilania telefonu		 	 			
		if(Tim_ge == 0)
		 {				
			HAL_GPIO_WritePin(RES_CE_GPIO_Port, RES_CE_Pin, GPIO_PIN_RESET); 			 			
			HAL_GPIO_WritePin(EN_GPIO_Port, EN_Pin, GPIO_PIN_SET);
			HAL_UART_MspInit(&huart1);	  
		  Tim_ge = 5000;
			Stan_ge++;			
		 }		 
		break;				 
		 
/******************************************************************************/	  						
		
		case 0x03:	 												// mon on				
		if(Tim_ge == 0)
		 {
			HAL_GPIO_WritePin(MON_GPIO_Port, MON_Pin, GPIO_PIN_SET);
      Tim_ge = 5000;
			Stan_ge++;			
		 }	 		 
		break;				 
		 
/******************************************************************************/	  							 		 		 	 		 
		 
	  case 0x04: 													// ATE0
		if(Tim_ge == 0)
		 {			  
			Send_gsm(Tab_ate0);			
			Tim_ge =300; 	
			Stan_ge++;			
		 }		
		break;				   
		 
/******************************************************************************/	  							 		 		 
		 		 		 
		case 0x05: 												 // AT+CMEE
		if(Tim_ge == 0)
		 {			 	
			Send_gsm(Tab_cme);						 
			Tim_ge =300; 	
			Stan_ge++;			
		 }		
		break;				    
		 
/******************************************************************************/	  							 		 		 		 		 
		 
	  case 0x06: 												 // AT+SIMD
		if(Tim_ge == 0)
		 {			 	
			Send_gsm(Tab_simd);						 
			Tim_ge =10000; 	
			Stan_ge++;			
		 }			
		break;				    		 
		 
/******************************************************************************/	  							 		 		 		 
		 
		case 0x07: 												  // AT+CPIN?			
	  if(Tim_ge == 0)
		 {			 				
		  Send_gsm(Tab_cpin);						 			 
			Tim_ge =1000; 	 
			Stan_ge++;		 						
		 }		
		break;				    
		 
/******************************************************************************/	  							 		 		 		 		 
		 
		case 0x08: 
		if(Test_dtgsm(Tab_ready))			  // oczekiwanie na odpowiedz o stan pin						
		 {
			Tim_ge =200; 									// brak wymagania pinu
			Stan_ge +=2;		 						 			
		 }		
			
    if(Test_dtgsm(Tab_simpin))									
		 {
			Tim_ge =200; 									// podanie pinu
			Stan_ge++;		 								 
		 }		
 
    if(Tim_ge == 0)
		 {			 										 								  
			Tim_ge =200; 									// restart
			Stan_ge =0;
			return; 
		 }			
		break;				    
		 
/******************************************************************************/	  							 		 		 		 		 
		 
		case 0x09: 										  // AT+CPIN=
	  if(Tim_ge == 0)
		 {			 				
		  Send_gsm(Tab_pin5664);						 			 			 
			Tim_ge =1500; 	 
			Stan_ge++;		 						
		 }		
		break;				     
				 
/******************************************************************************/	  							 		 		 		 		 
		 
		case 0x0A: 												// AT+CGSN			
	  if(Tim_ge == 0)
		 {			 	
			Cnt_imei =14; 
		 Send_gsm(Tab_csg);						 			 
			Tim_ge =500; 	 
			Stan_ge++;		 			
		 }		
		break;				    
		 		
/******************************************************************************/	  							 		 		 		 
		 
		case 0x0B: 												// odczyt IMEI
			
		if(Cnt_imei == 0xFF)
		 {	
			Tim_ge =20000; 	
			Stan_ge++;		 			
			return; 
		 } 		 
		if(Tim_ge == 0)
		 {			 	 	
		  Stan_ge =0;			 	 			
		 }	 		
		break;				     

/******************************************************************************/	  							 		 		 		 		 		 
		 
	  case 0x0C: 												// konfiguracja polaczenia TCP
				
	  if(Tim_ge == 0)
		 {			 				
		  Send_gsm(Tab_SetTCP);						 			 
			Tim_ge =500; 	
			Stan_ge++;			
		 }		
		break;				    
		 
/******************************************************************************/	  							 		 		 		 		 		 
		 
		case 0x0D: 											 // oczekiwanie na potwierdzenie zapisu konfiguracji TCP
		Rd_AtOk();						
		break;				  

/******************************************************************************/	  							 		 		 		 		 		 		
		
		case 0x0E: 												// konfiguracja polaczenia TCP2
	  if(Tim_ge == 0)
		 {			 				
		  Send_gsm(Tab_SetCfg2);						 			 
			Tim_ge =500; 	
			Stan_ge++;			
		 }		
		break;				    
		
/******************************************************************************/	  							 		 		 		 		 		 				
		
		case 0x0F: 											 // oczekiwanie na potwierdzenie zapisu konfiguracji TCP2
		Rd_AtOk();		
		break;				  

/******************************************************************************/	  				
				 
		case 0x10: 												// configuracja kontekstu		
					
		if(Tim_ge == 0)
		 {					
			i = Write_bfg(Tab_cgd);						 			  			
			Buf_i2c[0] = (uint8_t) (Apn_N >> 8); 	 // odczyt APN
	    Buf_i2c[1] = (uint8_t) (Apn_N);   	  		      
			Rd_i2c(Buf_i2c, 32);			
			for(j=0; j<32; j++)
			 {
				if(Buf_i2c[j] >= 0x20)
				 {	
					BufTxGsm[i++] = Buf_i2c[j];
				 }	 
			  else
				 {			
					break;
				 }
			 }					
			BufTxGsm[i++] = '"';
			BufTxGsm[i++] = 13; 
			BufTxGsm[i++] = 10;  
			U_TxCnt = i;
	    U_Txpnt = 0; 			
			FLAGS0 &= ~Ok_gpr; 
			#ifdef Test_gsm  
			CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
			#endif  	  	
			Tim_ge =5000; 		
			Stan_ge++;			
		 }		
		break;				 
		 
/******************************************************************************/	  							 		 		 		 		 		 
		 
		case 0x11: 												// oczekiwanie na potwierdzenie zapisu kontekstu
		Rd_AtOk();		
		break;				  
		 
/******************************************************************************/	  							 		 		 		 		 		 		
		
		case 0x12: 												// configuracja nazwy uzytkownika
		if(Tim_ge == 0)
		 {					
			i = Write_bfg(Tab_usr);						 			  				
			Buf_i2c[0] = (uint8_t) (User_N >> 8); 	 // odczyt UserN
	    Buf_i2c[1] = (uint8_t) (User_N);   	  		      
			Rd_i2c(Buf_i2c, 32);			
			for(j=0; j<32; j++)
			 {
				if(Buf_i2c[j] >= 0x20)
				 {	
					BufTxGsm[i++] = Buf_i2c[j];
				 }	 
			  else
				 {			
					break;
				 }
			 }					
			BufTxGsm[i++] = '"';
			BufTxGsm[i++] = 13; 
			BufTxGsm[i++] = 10;  
			U_TxCnt = i;
			U_Txpnt = 0;
			FLAGS0 &= ~Ok_gpr; 
			#ifdef Test_gsm  
			CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
			#endif  	  	
			Tim_ge =5000; 		
			Stan_ge++;			
		 }		
		break;				 	 
		 
/******************************************************************************/	  							 		 		 		 		 		 		

		case 0x13: 												// oczekiwanie na potwierdzenie zapisu uzytkownika
		Rd_AtOk();
		break;				  
		 
/******************************************************************************/	  							 		 		 		 		 		 		

		case 0x14: 												// configuracja hasla uzytkownika
		if(Tim_ge == 0)
		 {					
			i = Write_bfg(Tab_pas);					
			Buf_i2c[0] = (uint8_t) (Pass_N >> 8); 	 // odczyt UserP
	    Buf_i2c[1] = (uint8_t) (Pass_N);   	  		      
			Rd_i2c(Buf_i2c, 32);			
			for(j=0; j<32; j++)
			 {
				if(Buf_i2c[j] >= 0x20)
				 {	
					BufTxGsm[i++] = Buf_i2c[j];
				 }	 
			  else
				 {			
					break;
				 }
			 }					
			BufTxGsm[i++] = '"';
			BufTxGsm[i++] = 13; 
			BufTxGsm[i++] = 10;  
			U_TxCnt = i;
			U_Txpnt = 0;
			FLAGS0 &= ~Ok_gpr; 
			#ifdef Test_gsm  
			CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
			#endif  	  	
			Tim_ge =5000; 		
			Stan_ge++;			
		 }		
		break;				 	 
		
/******************************************************************************/	  							 		 		 		 		 		 		

		case 0x15: 												// oczekiwanie na potwierdzenie zapisu nazwy uzytkownika
		Rd_AtOk();
		break;				  
		 
/******************************************************************************/	  							 		 		 		 		 		 		
		
		case 0x16: 												// aktywacja kontekstu	
		if(Tim_ge == 0)
		 {			 	
			Send_gsm(Tab_act);						 			 
			Tim_ge =5000; 	
			Stan_ge++;			
		 }		
		break;				 
		 
/******************************************************************************/	  							 		 		 		 		 		 				 
		 
		case 0x17: 											  // oczekiwanie na aktywacje kontekstu	
			
		if(Test_dtgsm(Tab_sga))			
		 {	
			Tim_ge =5000; 		
		  Stan_ge++;	
			return; 
		 }
		 
		if(Tim_ge == 0)
		 {			 			 
		  if(Ctn_errKnt < 5)
		   {				 
		    Tim_ge =15000; 									// ponowna aktywacja kontekstu za 15s	
		    Stan_ge =0x16;	
		    Ctn_errKnt++; 			 
		   }
	    else	
		   {	
			  Tim_ge =200; 		
			  Stan_ge =0;	 
			  Ctn_errKnt =0; 			 
		   }
		 }			 			 		
		break;				   
		 
/******************************************************************************/	  							 		 		 		 		 		 				 
		
		case 0x18: 												// kontekst zaktywowany, oczekiwanie na dane do wyslania											
																			// czy sa dane do wyslania, proba na IP primary
		if((Il_gprs != 0) && (Tim_ge == 0))	
		 {			 			 	
			if(Atc_srv == 0xFF)			  			// gdy brak polaczenia, to bez sprawdzania jego statusu przejscie do nawiazania polaczenia 	
			 {			
				Tim_ge =200; 
				Stan_ge =0x1A;											
				return;			
			 }	 
				 			 
			if(Nr_srv != Atc_srv) 				  // gdy biezace polaczenie nie jest zgodne z wymaganym, to rozlaczenie
			 {	
			  Send_gsm(Tab_sh);						 	// polecenie rozlaczenia polaczenia
			  Tim_ge =2000; 	
			  Stan_ge =0x1A;							
			 }
			else 
			 {					
		    Send_gsm(Tab_ss);						  // gdy biezace polaczenie jest zgodne z wymaganym, to test stanu polaczenia
			  Tim_ge =2000; 	
			  Stan_ge++;
		   }
	   }
    else
		 {	
			if((Tim_csq == 0) && (Tim_ge == 0))
			 {	
				Tim_csq = T_csq; 
				Send_gsm(Tab_csq); 
			 }	 
		 }	 		 
	 	break;				   	 
			 
/******************************************************************************/
		 
		case 0x19: 		 
			
		if(Test_dtgsm(Tab_sts))						
		 {
			if(BufIdlGE[7] == 0x30)												 // czy brak polaczenia	
			 {	
				Tim_ge =200; 																 // przejscie do nawiazania polaczenia
				Stan_ge++;											
				return;			
			 }	 
				 
			if(BufIdlGE[7] > 0x30)												 // czy jest polaczenie
			 {	
				Tim_ge =200; 																 // przejscie do wysylania danych
				Stan_ge =0x1C;											
				return;			
			 }
		 }		
					
    if(Tim_ge == 0)
		 {			 	
			Send_gsm(Tab_sh);						 								   // polecenie rozlaczenia polaczenia
			Tim_ge =2000; 	
			Stan_ge++;
			return; 
		 }			
		break;				   
		
/******************************************************************************/	  							 		 		 		 		 		 				 		
		 
		case 0x1A:
			
		if(Tim_ge == 0)
		 {			 	 
			Buf_i2c[0] = (uint8_t) ((IP_prim + (Nr_srv * 4)) >> 8);    // 4B IP
	    Buf_i2c[1] = (uint8_t)  (IP_prim + (Nr_srv * 4));
      Rd_i2c(Buf_i2c,4);	 
			IP_gprs[0] = Buf_i2c[0];
			IP_gprs[1] = Buf_i2c[1]; 
			IP_gprs[2] = Buf_i2c[2]; 
			IP_gprs[3] = Buf_i2c[3]; 			 
			 
			Buf_i2c[0] = (uint8_t) ((Port_prim + (Nr_srv * 2)) >> 8);  // 2B Port
	    Buf_i2c[1] = (uint8_t)  (Port_prim + (Nr_srv * 2));   	 
      Rd_i2c(Buf_i2c,2);	  
			Port_gprs  = (uint16_t) (Buf_i2c[0] << 8);
			Port_gprs |= (uint16_t) (Buf_i2c[1]); 
			 
			i = Write_bfg(Tab_Con);						
			ByteToAsc(Port_gprs);
			i = Write_AscBf(i);
			BufTxGsm[i++] = ',';
			BufTxGsm[i++] = '"';
			 			
			ByteToAsc(IP_gprs[0]); 
			i = Write_AscBf(i);			
			BufTxGsm[i++] = '.';
			ByteToAsc(IP_gprs[1]); 
			i = Write_AscBf(i);			
			BufTxGsm[i++] = '.';
			ByteToAsc(IP_gprs[2]); 
			i = Write_AscBf(i);			
			BufTxGsm[i++] = '.';
			ByteToAsc(IP_gprs[3]); 
			i = Write_AscBf(i);
			BufTxGsm[i++] = '"';
			BufTxGsm[i++] = ',';
			BufTxGsm[i++] = '0';
			BufTxGsm[i++] = ',';
			BufTxGsm[i++] = '0';
			BufTxGsm[i++] = ',';
			BufTxGsm[i++] = '1';			 
			BufTxGsm[i++] = 13; 
			BufTxGsm[i++] = 10;  
			U_TxCnt = i;
			U_Txpnt = 0;
			FLAGS0 &= ~Ok_gpr; 
			#ifdef Test_gsm  
			CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
			#endif  				 	
			Tim_ge =5000; 		
			Stan_ge++;			
		 }		
		break; 
		 		
/******************************************************************************/	  							 		 		 		 		 		 				 				 
		 
		case 0x1B:									// oczekiwanie na polaczenie
		if(FLAGS0 & Ok_gpr)
	   {	
			FLAGS0 &= ~Ok_gpr; 
		  Tim_ge =200; 							// jest polaczenie
		  Stan_ge++;			
			Cnt_errC =0;
			Cnt_srv =0; 	
			Atc_srv = Nr_srv;
			return; 
	   }		
    	
		if(Tim_ge == 0)	  		  		// brak polaczenia
		 {
			if(Cnt_errC < 16) 
			 {	
			  Cnt_errC++;
			 }	 
			else
			 {		
				Cnt_errC =0;
				Tim_ge =200; 
				Stan_ge =0;
				return; 
			 }	 
							 
		  if(Cnt_srv < 3)					 // po 3 probach przelaczenie serwera na drugi 
			 {	
			  Cnt_srv++;
				Tim_ge =1000; 							
				Stan_ge =0x1A;			 
			 }	 
		  else
			 {		
				Cnt_srv =0; 
				Nr_srv ^= 0x01;				
				Stan_ge = 0x18;			  
				Atc_srv = 0xFF;
			 }				    
		 }			
		break;  
		 
/******************************************************************************/
		 
		case 0x1C:									// wysylanie danych
		if(Tim_ge == 0)	
		 {
			i = Write_bfg(Tab_SendE);		
			ByteToAsc(Il_gprs);		
		  i = Write_AscBf(i);
			BufTxGsm[i++] = 13; 			
			U_TxCnt = i;
			U_Txpnt = 0;
			FLAGS0 |= Rx_znTX;
			FLAGS0 &= ~Ok_gpr; 			 
			#ifdef Test_gsm  
			CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
			#endif  				 				
			Tim_ge =200; 		
			Stan_ge++;			 
		 }					
		break;
			
/******************************************************************************/
		 
		case 0x1D:		
			
		if((FLAGS0 & Rx_znTX) == 0x00)
		 {	 		  
			for(i=0; i<300; i++)
			 {
			  BufTxGsm[i] = Buf_gprs[i];
			 }			 
			U_TxCnt = Il_gprs;
			U_Txpnt = 0; 
			FLAGS0 &= ~Ok_gpr; 
			#ifdef Test_gsm  
			CDC_Transmit_FS(Buf_gprs, Il_gprs); 			 			  						 	
			#endif  			
	 		Tim_ge =1000; 	  
			Stan_ge++;			  			 
			return;
		 }
		 
		if(Tim_ge == 0)
		 {			 	
			Send_gsm(Tab_sh);						 								   // polecenie rozlaczenia polaczenia
			Tim_ge =2000; 	
			Stan_ge =0x18;							
			return; 
		 }			 
		break; 
		 
/******************************************************************************/		 
		 
		case 0x1E:		 			
			
		if(FLAGS0 & Ok_gpr)
	   {	
			FLAGS0 &= ~Ok_gpr; 
		  Il_gprs =0;								 									// dane wyslane	
			Tim_ge =100; 	   
		  Stan_ge =0x18;									
			return; 
	   }		
    	
		if(Tim_ge == 0)	  		  		                 // brak potwierdzenia wyslania danych
		 {
			Il_gprs =0; 
		  Stan_ge =0x18;									
			Tim_ge =100; 
		 }				
		break;  
		 		
/******************************************************************************/		 		 		 
  }	
}

/*******************************************************************************
 Funkcja wysylania komend at do GE910
*******************************************************************************/ 
 
void Send_gsm(const uint8_t tablica[])
 {
	uint16_t i=0;

  for(i=0; i<300; i++)
	 {
		if(tablica[i] != 0)
		 {		
      BufTxGsm[i] = tablica[i]; 
		 }
		else
		 {		
			break;
		 }		
	 }
	FLAGS0 &= ~Rx_ge; 
	FLAGS0 &= ~Ok_gpr; 
	 
	U_TxCnt = i;
	U_Txpnt = 0; 	 	
	#ifdef Test_gsm  
	CDC_Transmit_FS(BufTxGsm,i); 			 			  						 	
	#endif  	 
 }	 

/*******************************************************************************
 Zapis bufora tx gsm
*******************************************************************************/ 
 
uint16_t Write_bfg(const uint8_t tablica[])	
 {
  uint16_t i=0;

  for(i=0; i<300; i++)
	 {
		if(tablica[i] != 0)
		 {		
      BufTxGsm[i] = tablica[i]; 
		 }
		else
		 {		
			break;
		 }		
	 }
	FLAGS0 &= ~Rx_ge; 
	return i; 
 }	
 
/*******************************************************************************
 Odbior danych z ge910
*******************************************************************************/

void Rx_ge910(uint8_t Data)
 {
  uint16_t i=0; 
	 
	if(FLAGS0 & Rx_znTX)
	 {	
	  if(Data == '>')
		 {	
			FLAGS0 &= ~Rx_znTX;
			return; 
		 }		
	 }
		 
	if(Cnt_imei != 255)
	 {	
		if(Data > 0x20)
		 {		
		  Buf_imei[15 - Cnt_imei] = Data;		  
			Cnt_imei--; 		 
		 }	 
   }	 
  else
	 {			 	
    if((Data < 0x20) || (Ind_RxIrqGE >= 1095))
	   {
		  if(Ind_RxIrqGE != 0)
		   {
				if((BufIrqGE[0] == 'O') && (BufIrqGE[1] == 'K'))
			   {	 				  
					#ifdef Test_gsm			 
			    CDC_Transmit_FS(BufIrqGE,Ind_RxIrqGE); 			 			  						 	
			    #endif  	 					 					 
					for(i=0; i<4; i++)
			     { 			      
				    BufIrqGE[i] =0; 				 
			     }	 			    
			    Ind_RxIrqGE =0; 			 		      
					FLAGS0 |= Ok_gpr; 			    
		     }
				else
				 {						 		 			
			    for(i=0; i<(Ind_RxIrqGE+2); i++)
			     { 
			      BufIdlGE[i] = BufIrqGE[i];
				    BufIrqGE[i] =0; 				 
 			     }	 
			    Ind_RxIdlGE = Ind_RxIrqGE;
			    Ind_RxIrqGE =0; 			 
		      FLAGS0 |= Rx_ge;
			    #ifdef Test_gsm			 
			    CDC_Transmit_FS(BufIdlGE,Ind_RxIdlGE); 			 			  						 	
			    #endif  	 
		     }		 
			 }	 
	   } 		 
	  else
	   {
		  BufIrqGE[Ind_RxIrqGE] = Data; 
		  Ind_RxIrqGE++;				 
	   }	 
   }		 
 }	 

/*******************************************************************************
 Funkcja testujaca odebrany ciag
*******************************************************************************/ 
 
uint8_t Test_dtgsm(const uint8_t tablica[])
 {
	uint16_t i=0; 
  if(FLAGS0 & Rx_ge)
	 {	
		for(i=0; i<299; i++)
		 {
			if(tablica[i] == 0)
			 {				
				return 1;
			 }	 
			if(BufIdlGE[i] !=  tablica[i])
			 {						
			  return 0;
			 }	 
		 }  
	 }	
  return 0; 	 
 } 

/*******************************************************************************
 Oczekiwanie potwierdzeia konfiguracji
*******************************************************************************/
  
void Rd_AtOk(void)
 {		
  if(FLAGS0 & Ok_gpr)
	 {
		FLAGS0 &= ~Ok_gpr; 			    	
		Tim_ge =200; 		
		Stan_ge++;			
	 }		
  else
	 {		
		if(Tim_ge == 0)
		 {					
		  Tim_ge =200; 		
			Stan_ge =0;	 
		 }	 
	 }
 }		 
 
/*******************************************************************************
 Zapis danych do pamieci i2c
*******************************************************************************/ 
 
void Wr_i2c(uint8_t *Buf, uint8_t Ilosc)
 {
	if(!(HAL_I2C_IsDeviceReady(&hi2c1, 0xA0, 500, 10)))
	 {	
    HAL_I2C_Master_Transmit(&hi2c1,0xA0, Buf, Ilosc+2, 50);	 	
	 }	 
 }	 

/*******************************************************************************
 Zapis danych do pamieci i2c adresowanie rozszerzone 0x00000 - 0x1FFFF 
*******************************************************************************/ 

void Wr_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc)
 {
  uint8_t i=0;
	uint8_t Adr=0; 
	 
	if(Adress & 0x00010000) 
	 {	
	  Adr = 0xA2;
	 }
  else 	 
	 {	
		Adr = 0xA0; 
	 }	 
	 
	if(!(HAL_I2C_IsDeviceReady(&hi2c1, 0xA0, 500, 10)))	  
	 { 	
		TmpI2C[0] = (uint8_t) (Adress >> 8);
	  TmpI2C[1] = (uint8_t) (Adress);
		 
		for(i=0; i<32; i++)
     {		 
		  TmpI2C[2+i] = Buf[i];
		 }	  		 
	  HAL_I2C_Master_Transmit(&hi2c1, Adr, TmpI2C, Ilosc+2, 50);	 	
	 }	 
 }	
 
/*******************************************************************************
 Odczyt danych z pamieci i2c
*******************************************************************************/ 
 
void Rd_i2c(uint8_t *Buf, uint8_t Ilosc)
 {
	if(!(HAL_I2C_IsDeviceReady(&hi2c1, 0xA0, 500, 10))) 
	 {		 	  
    HAL_I2C_Master_Transmit(&hi2c1, 0xA0, Buf, 2, 50);	 	 
    HAL_I2C_Master_Receive(&hi2c1, 0xA1, Buf, Ilosc, 50);		 
	 }	 
 }	 
 
/*******************************************************************************
 Odczyt danych z pamieci i2c adresowanie rozszerzone 0x00000 - 0x1FFFF 
*******************************************************************************/ 

void Rd_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc)
 {
	uint8_t Adr=0; 
	 
	if(Adress & 0x00010000) 
	 {	
	  Adr = 0xA2;
	 }
  else 	 
	 {	
		Adr = 0xA0; 
	 }	 
	 
	if(!(HAL_I2C_IsDeviceReady(&hi2c1, 0xA0, 500, 10)))	  
	 { 	
		TmpI2C[0] = (uint8_t) (Adress >> 8);
	  TmpI2C[1] = (uint8_t) (Adress);
		HAL_I2C_Master_Transmit(&hi2c1, Adr, TmpI2C, 2, 50);	 	  
		HAL_I2C_Master_Receive(&hi2c1, (Adr | 0x01), Buf, Ilosc, 50);		  
	 }	 	 
 }	 
 
/*******************************************************************************
 Konwersja ByteToAsc
*******************************************************************************/

void ByteToAsc(uint16_t Value)
 {
  uint8_t Z1,Z2,Z3,Z4,Z5;
  
	Z1 = Value / 10000; Value = Value - ( Z1* 10000) ; Z1 += '0'; 
  Z2 = Value /  1000; Value = Value - ( Z2* 1000 ) ; Z2 += '0';
  Z3 = Value /   100; Value = Value - ( Z3*  100 ) ; Z3 += '0';
  Z4 = Value /    10; Value = Value - ( Z4*   10 ) ; Z4 += '0';
  Z5 = Value       ;                               ; Z5 += '0';
   
  ByteToAscStr[0] =  Z1;
	ByteToAscStr[1] =  Z2;
  ByteToAscStr[2] =  Z3;
  ByteToAscStr[3] =  Z4;
  ByteToAscStr[4] =  Z5;	 
 }
 
/*******************************************************************************
 Zapis do bufora 
*******************************************************************************/ 
 
uint8_t Write_AscBf(uint8_t Indeks)
 { 
  if(ByteToAscStr[0] != 0x30)
	 {		
	  BufTxGsm[Indeks++] = ByteToAscStr[0];
		BufTxGsm[Indeks++] = ByteToAscStr[1];   
		BufTxGsm[Indeks++] = ByteToAscStr[2];   
		BufTxGsm[Indeks++] = ByteToAscStr[3];   
		BufTxGsm[Indeks++] = ByteToAscStr[4];     
		return Indeks;
	 }		
	
  if((ByteToAscStr[0] == 0x30) && (ByteToAscStr[1] != 0x30)) 
	 {	
	  BufTxGsm[Indeks++] = ByteToAscStr[1];   
		BufTxGsm[Indeks++] = ByteToAscStr[2];   
		BufTxGsm[Indeks++] = ByteToAscStr[3];   
		BufTxGsm[Indeks++] = ByteToAscStr[4];     
		return Indeks;
	 }	 
		 
	if((ByteToAscStr[0] == 0x30) && (ByteToAscStr[1] == 0x30) && (ByteToAscStr[2] != 0x30)) 
	 {		
		BufTxGsm[Indeks++] = ByteToAscStr[2];   
		BufTxGsm[Indeks++] = ByteToAscStr[3];   
		BufTxGsm[Indeks++] = ByteToAscStr[4];     
		return Indeks;
	 }	  
	 
	if((ByteToAscStr[0] == 0x30) && (ByteToAscStr[1] == 0x30) && (ByteToAscStr[2] == 0x30) && (ByteToAscStr[3] != 0x30)) 
	 {		 
	  BufTxGsm[Indeks++] = ByteToAscStr[3];   
		BufTxGsm[Indeks++] = ByteToAscStr[4];     
		return Indeks;
	 }	  
		 
	BufTxGsm[Indeks++] = ByteToAscStr[4];     
	return Indeks;
 }	   
 
/*******************************************************************************
 Konwersja poziomu sygnalu gsm
*******************************************************************************/ 
 
void Obs_csq(void)
 {
  uint8_t i,j =0; 
	 
  for(i=0; i<10; i++)
	 {
		if(BufIdlGE[i] == ',')
		 {	
		  break;
		 }
	 }	 
  		
  j = BufIdlGE[i - 1] & 0x0F;
	 
	if(BufIdlGE[i - 2] != ' ')
	 {		
	  j += (BufIdlGE[i - 2] & 0x0F) * 10;
	 }	 
	Rssi_gsm = j;
	 
	if(Rssi_gsm == 0)
	{		
	 i++; 
		
  }	
		
	if(Rssi_gsm <= 8)
	 {		
	  Mask_ld2 =0x80000000;
		return; 
	 }	 	 
	if(Rssi_gsm <= 16)
	 {		
	  Mask_ld2 =0x88000000;
		return; 
	 }	  
	if(Rssi_gsm <= 24)
	 {		
	  Mask_ld2 =0x88800000;
		return; 
	 }	   
	Mask_ld2 =0x88880000; 	 
 }	

/*******************************************************************************
 Konwersja odebranych danych z serwera
*******************************************************************************/ 
 
void Rx_stSrv(void)
 {
  uint16_t i =0;
	uint16_t j =0; 
	uint16_t k =0; 
		
  for(i=9; i<14; i++)
	 {
		if(BufIdlGE[i] == ',')
		 {	
		  break;
		 }
	 }	 
  
  k = BufIdlGE[i - 1] & 0x0F;      // wyznaczenie ilosci odebranych bajtow
	if(BufIdlGE[i - 2] != ',')
	 {		
    k += ((BufIdlGE[i - 2] & 0x0F) * 10);
		if(BufIdlGE[i - 3] != ',')
		 {	
		  k += ((BufIdlGE[i - 3] & 0x0F) * 100);
		 }	 
	 }
  
	while(j < k) 	 
   { 	 
    BufIdlGETmp[j]  = (Knw_bin(BufIdlGE[0 + i + 1])) << 4;		 
		BufIdlGETmp[j] |=  Knw_bin(BufIdlGE[1 + i + 1]);		 
		j++; 
		i += 2;
   }
	 
	BufIdlGE[0] =0; 
	BufIdlGE[1] =0;  
	BufIdlGE[2] =0;  
  BufIdlGE[3] =0; 	
  Il_srv = k; 	 
  FLAGS1 |= Rx_srv; 
 }

/*******************************************************************************
 Zamianana ascii na bin
*******************************************************************************/ 
 
uint8_t Knw_bin(uint8_t Data)
 {
  if((Data >= 0x30) && (Data <= 0x39))
	 {		
	  return (Data & 0x0F);
	 }	 
  
	if((Data >= 0x41) && (Data <= 0x46))
	 {			 
	  return (Data - 0x37);		 
	 }	 
  return 0;			
 }	 
 
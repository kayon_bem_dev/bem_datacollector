/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "s2lp.h"
#include "usb_device.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile uint8_t FLAGS0 =0; 
volatile uint8_t FLAGS1 =0; 
volatile uint16_t Wt_ms;
volatile uint16_t Licz_rmk;
volatile uint16_t IndM_wmb =0;
volatile uint16_t Snd_IndWmb =0;
uint8_t Buf_i2c[260] __attribute__((aligned(4)));
volatile uint16_t Il_gprs;
uint8_t Buf_gprs[300]__attribute__((aligned(4)));
volatile uint32_t Cnt_stat =0;
volatile uint16_t Snd_rq =0;
volatile uint16_t Snd_st = Stat_dtc | Knf_dct | White_L;

volatile uint16_t Ind_wht =0;
volatile uint8_t Stan_snd =0;
volatile uint8_t Nr_srv =0;	
volatile uint8_t Flt_tmp =0;
volatile uint32_t Mask_ld2 =0;
volatile uint32_t Rol_jld2 =0x80000000;
volatile uint8_t Cnt_100ms =0;
volatile uint16_t Tim_rq =0; 	 	
volatile uint32_t Cnt_set =0;
volatile uint32_t Tim_snd =0;
volatile uint16_t Ind_fw =0;
volatile uint32_t Tim_rstGE =0;
volatile uint16_t Tim_ld3 =4000;

#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off)
volatile uint8_t  Tab_sn[16];
volatile uint8_t  Tab_id[4096];
#endif

extern uint8_t WMB_Idbuf[270] __attribute__((aligned(4)));
extern volatile uint8_t Il_wmb;
extern uint8_t BufIdlGE[1100] __attribute__((aligned(4)));
extern uint16_t Ind_RxIdlGE;
extern uint8_t Buf_imei[32] __attribute__((aligned(4)));
extern uint8_t BufTxGsm[300] __attribute__((aligned(4)));
extern uint8_t BufIdlGETmp[550] __attribute__((aligned(4)));
extern volatile uint8_t Stan_ge;
extern volatile uint16_t U_TxCnt;
extern volatile uint16_t U_Txpnt;
extern volatile uint16_t Tim_csq; 
extern volatile uint8_t Rssi_gsm;
extern volatile uint16_t Il_srv;
extern volatile uint16_t Tim_ge;

/*******************************************************************************
 Naglowek dla ISP z wersja DCTL
*******************************************************************************/

const uint8_t WERISP[16] __attribute__((at(0x801FFF0))) = {0,0,0,0,0,0,0,0,0,0,0,0,0,Wer_mj, Wer_mi, Wer_rel};
	
/******************************************************************************/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Wait_ms(uint16_t Tim_ms);
void Wr_Mspi(uint32_t Adress, uint8_t *Buf, uint16_t Ilosc);
void Rd_Mspi(uint32_t Adress, uint8_t *Buf, uint16_t Ilosc);
void Wr_Mwmb(void);
void Snd_Mwmb(void);
void Set_test(void);	
void Send_Cstat(void);		
void Send_srw(void);
void Init_DtCnt(void);	
uint32_t calc_crc(uint8_t *Buf, uint16_t Size);
void Init_DtKnf(void);	
void Init_DtWht(void);	
void Send_prim(void);		
void Obs_txUart(void);
uint8_t Init_DtWmb(void);
void Obs_tim25ms(void);	
void Obs_tmp(void);	
void Obs_ld2(void);
void Wr_konfig(void);							
void Wr_whitel(void);							
void Init_RdFmw(void);
void Wr_firmware(void); 
void Init_RdCrcFw(void); 
void Obs_rstGE(void);	
uint8_t Test_whlt(void);
void Rd_whtl(void);
void Test_wrk(void); 				

extern void GE910_FN(void);
extern void Test_s2lp(void);	
extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
extern void Wr_i2c(uint8_t *Buf, uint8_t Ilosc);
extern void Wr_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc);
extern void Rd_i2c(uint8_t *Buf, uint8_t Ilosc);
extern void Rd_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc);

/******************************************************************************/

const uint8_t Tab_apn[32]  = {'i','n','t','e','r','n','e','t',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const uint8_t Tab_user[32] = {'i','n','t','e','r','n','e','t',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const uint8_t Tab_pasw[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
	
/******************************************************************************/	
	
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();	
	#if defined(Test_gsm) || defined(Test_wmb)	
  MX_USB_DEVICE_Init();	
	#endif	
  MX_I2C1_Init();
	#ifndef DEBUG
  MX_IWDG_Init();
	#endif
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();

  /* USER CODE BEGIN 2 */	
	
	Set_test();	    	 
	#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off)
	Rd_whtl();								// odczyt whitelist z pamieci	
	#endif
	while((Read_Spr(0x8E) >> 1) != 0x30)				
	 {		
		S2LP_INIT(); 	  
		S2LP_RX();	 
	 }	 

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	 GE910_FN();										// obsluga GE910
	 Obs_txUart();									// obsluga bufora nadawczego GE910	
		
	 Send_srw();										// funkcja wysylania danych do serwera	
		
	 if(FLAGS0 & Dt_wmb)
	  {
		 Wr_Mwmb();										// zapis odebranej ramki WMB do pamieci			
		 FLAGS0 &= ~Dt_wmb;				 		 			 
	  }		
								
	 if(FLAGS0 & mn25ms)
	  {		 
		 FLAGS0 &= ~mn25ms;		 		 		 		 
		 #ifndef DEBUG	
		 HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		 #endif				
		 Test_wrk(); 				
		 Obs_ld2();	
		 Test_s2lp();									 
		 Send_Cstat();		
		 Obs_tmp();	
		 Obs_tim25ms();	
		 Obs_rstGE();		
		}	
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 3, 0);
}

/* USER CODE BEGIN 4 */

/*******************************************************************************
 Funkcja opoznienia czasowego ms
*******************************************************************************/

void Wait_ms(uint16_t Tim_ms)
 {	
  Wt_ms = Tim_ms;	 
  while(Wt_ms != 0){}		
 }	 

/*******************************************************************************
 Zapis do ramu spi
*******************************************************************************/ 
 
void Wr_Mspi(uint32_t Adress, uint8_t *Buf, uint16_t Ilosc)
 {
	uint8_t Tmp_bf[4] = {0x02,(uint8_t)(Adress >> 16),(uint8_t)(Adress >> 8),(uint8_t)(Adress)}; 
	
	HAL_GPIO_WritePin(MCS_GPIO_Port, MCS_Pin, GPIO_PIN_RESET);					
	HAL_SPI_Transmit(&hspi2,Tmp_bf,4,5); 		 		
	HAL_SPI_Transmit(&hspi2,Buf,Ilosc,20);	
  HAL_GPIO_WritePin(MCS_GPIO_Port, MCS_Pin, GPIO_PIN_SET);					
 }	

/*******************************************************************************
 Odczyt z ramu spi
*******************************************************************************/
 
void Rd_Mspi(uint32_t Adress, uint8_t *Buf, uint16_t Ilosc)
 {
  uint8_t Tmp_bf[4] = {0x03,(uint8_t)(Adress >> 16),(uint8_t)(Adress >> 8),(uint8_t)(Adress)}; 
	
	HAL_GPIO_WritePin(MCS_GPIO_Port, MCS_Pin, GPIO_PIN_RESET);					 
	HAL_SPI_Transmit(&hspi2,Tmp_bf,4,5); 		 		
	HAL_SPI_Receive(&hspi2, Buf,Ilosc,20);	
  HAL_GPIO_WritePin(MCS_GPIO_Port, MCS_Pin, GPIO_PIN_SET);						
 }	
 
/*******************************************************************************
 Buforowanie ramki WMB
*******************************************************************************/ 
 
void Wr_Mwmb(void)
 {
	uint16_t i=0;
	uint16_t j=0; 
	 
	Tim_ld3 =20;	 				  	   	  
	 
	for(j=0; j<1024; j++)														// test czy jest juz rekord tego licznika w pamieci
	 {	
	  Rd_Mspi((uint32_t)(j << 8), Buf_i2c, 256);
		if((Buf_i2c[2] == 0x44) && (Buf_i2c[3] == WMB_Idbuf[3]) && (Buf_i2c[4] == WMB_Idbuf[4]) && (Buf_i2c[5] == WMB_Idbuf[5]) && (Buf_i2c[6] == WMB_Idbuf[6]) && (Buf_i2c[7] == WMB_Idbuf[7]) && (Buf_i2c[8] == WMB_Idbuf[8])) 
		 {	
		  for(i=0; i<256; i++)											  // jest rekord tego licznika, aktualizacja jego stanu 			
			 {	 
				Buf_i2c[i] =0;	 
			 }	   
			Wr_Mspi((uint32_t)(j << 8), Buf_i2c, 256); 
			Wr_Mspi((uint32_t)(j << 8), WMB_Idbuf, Il_wmb); 
			#ifdef Test_wmb 			
			CDC_Transmit_FS(WMB_Idbuf,Il_wmb); 			 			  						
			#endif		   
			return; 
		 }
	 }	 
	 
	#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off)	 	 	 
	if((FLAGS0 & Trb_rst) == 0)
	 {	 
	  if(Test_whlt() == 0) 													// filtrowanie z whitelistami
	   {																						// gdy brak licznika na liscie to bez zapisu do pamieci		
	    return;
	   }	 
	 }	 		
	#endif 
	 
  for(i=0; i<256; i++)												  // nowy wpis rekordu do bufora
   {	 
    Buf_i2c[i] =0;	 
	 }	   
	Wr_Mspi((uint32_t)(IndM_wmb << 8), Buf_i2c, 256);	
	Wr_Mspi((uint32_t)(IndM_wmb << 8), WMB_Idbuf, Il_wmb);	
	IndM_wmb = (IndM_wmb + 1) & 0x03FF; 
	#ifdef Test_wmb 			
	CDC_Transmit_FS(WMB_Idbuf,Il_wmb); 			 			  						
	#endif		  
 }	 
 
/*******************************************************************************
 Kontrola zainicjowania pamieci ustawien
*******************************************************************************/ 

void Set_test(void)
 {
	uint8_t i=0; 
	
	Buf_i2c[0] = (uint8_t) (ID_set >> 8); 
	Buf_i2c[1] = (uint8_t) (ID_set);   	 
  Rd_i2c(Buf_i2c, 2);	
	 
	#ifdef Set_ini
	Buf_i2c[0] =0; 
	Buf_i2c[1] =0;

	Buf_i2c[0] = (uint8_t) (ID_Fw >> 8); 			 // 4B IP primary 	       
	Buf_i2c[1] = (uint8_t) (ID_Fw);   	 	
	Buf_i2c[2] =  0xFF;	
	Wr_i2c(Buf_i2c, 1); 
	#endif 
	 
	if((Buf_i2c[0] != ((uint8_t) (ID_sval >> 8))) || (Buf_i2c[1] != ((uint8_t) (ID_sval))))
	 {	
	 	Buf_i2c[0] = (uint8_t) (ID_set >> 8);        // znacznik zainicjowania ustawien
	  Buf_i2c[1] = (uint8_t) (ID_set);   	 
		Buf_i2c[2] = (uint8_t) (ID_sval >> 8); 
	  Buf_i2c[3] = (uint8_t) (ID_sval);   	  
		Wr_i2c(Buf_i2c, 2);

		Buf_i2c[0] = (uint8_t) (IP_prim >> 8); 			 // 4B IP primary 	       
	  Buf_i2c[1] = (uint8_t) (IP_prim);   	 
		Buf_i2c[2] =  IP_prim_val3;
	  Buf_i2c[3] = 	IP_prim_val2;
		Buf_i2c[4] =  IP_prim_val1;
	  Buf_i2c[5] =  IP_prim_val0;		 
		Wr_i2c(Buf_i2c, 4);
		 
		Buf_i2c[0] = (uint8_t) (IP_sec >> 8); 			 // 4B IP secondary 	       
	  Buf_i2c[1] = (uint8_t) (IP_sec);   	 
		Buf_i2c[2] =  IP_sec_val3;
	  Buf_i2c[3] = 	IP_sec_val2;
		Buf_i2c[4] =  IP_sec_val1;
	  Buf_i2c[5] =  IP_sec_val0;		 
		Wr_i2c(Buf_i2c, 4); 
		 
		Buf_i2c[0] = (uint8_t) (Port_prim >> 8); 		 // 2B Port primary
	  Buf_i2c[1] = (uint8_t) (Port_prim);   	 
		Buf_i2c[2] = (uint8_t) (Port_prim_val >> 8);
	  Buf_i2c[3] = (uint8_t) (Port_prim_val);
		Wr_i2c(Buf_i2c, 2);  
		 
		Buf_i2c[0] = (uint8_t) (Port_sec >> 8); 		 // 2B Port secondary
	  Buf_i2c[1] = (uint8_t) (Port_sec);   	 
		Buf_i2c[2] = (uint8_t) (Port_sec_val >> 8);
	  Buf_i2c[3] = (uint8_t) (Port_sec_val);
		Wr_i2c(Buf_i2c, 2);   
		
		Buf_i2c[0] = (uint8_t) (Port_sec >> 8); 		 // 2B Port secondary
	  Buf_i2c[1] = (uint8_t) (Port_sec);   	 
		Buf_i2c[2] = (uint8_t) (Port_sec_val >> 8);
	  Buf_i2c[3] = (uint8_t) (Port_sec_val);
		Wr_i2c(Buf_i2c, 2);    
		 
		Buf_i2c[0] = (uint8_t) (Apn_N >> 8); 	     	 // 32B APN
	  Buf_i2c[1] = (uint8_t) (Apn_N);   	  		
    for(i=0; i<32; i++)
		 {	
			Buf_i2c[2+i] = Tab_apn[i]; 
		 } 
		Wr_i2c(Buf_i2c, 32);
		 
		Buf_i2c[0] = (uint8_t) (User_N >> 8); 	     // 32B User_N
	  Buf_i2c[1] = (uint8_t) (User_N);   	  		
    for(i=0; i<32; i++)
		 {	
			Buf_i2c[2+i] = Tab_user[i]; 			 
		 } 
		Wr_i2c(Buf_i2c, 32); 
		 
		Buf_i2c[0] = (uint8_t) (Pass_N >> 8); 	     // 32B Password_N
	  Buf_i2c[1] = (uint8_t) (Pass_N);   	  		
    for(i=0; i<32; i++)
		 {	
			Buf_i2c[2+i] = Tab_pasw[i]; 			 
		 } 
		Wr_i2c(Buf_i2c, 32);  	 
	 }
 }	

/*******************************************************************************
 Wysylanie statusu koncentratora do serwera
*******************************************************************************/
  
void Send_Cstat(void)
 {
	uint32_t T_stat = Tim_stat; 
	 
	if(FLAGS0 & Trb_rst)
	 {	
		T_stat = 2400;
	 }		

	if(Cnt_stat < T_stat)
	 {		
	  Cnt_stat++;
	 }	 
	else
	 {
		Cnt_stat =0;
		Snd_st |= Stat_dtc;		 		
	 }
 } 	 
		 
/*******************************************************************************
 Funkcja wysylania danych do serwera
*******************************************************************************/
 
 void Send_srw(void)
  {
	 switch(Stan_snd)
	  {	
/******************************************************************************/	  
		 
		 case 0x00:	 												// test zgloszen danych do wysylania
		
		 if((Stan_ge >= 0x17) && (Il_gprs == 0))
		  {   				
			 if(Snd_st & Stat_dtc)
			  {  
				 Init_DtCnt();				 								 
				 Snd_rq |= Stat_dtc;	
				 Stan_snd++;					 							 
			   Tim_rq =1000; 	 	
				 return; 	
				}		 
				
			 if(Snd_st & Knf_dct)	
			  {  
				 Init_DtKnf();				
				 Snd_rq |= Knf_dct;			
				 Stan_snd++;					 
				 Tim_rq =1000; 	 	
				 return; 	
				}  
			 
			 if(Snd_st & White_L)	
			  {  
				 Init_DtWht();
				 Snd_rq |= White_L;			
				 Stan_snd++;					 
				 Tim_rq =1000; 	 		
				 return; 	
				}  				
			
			 if(Snd_st & Rd_crc)	
			  {  
				 Init_RdCrcFw();
				 Snd_rq |= Rd_crc;			
				 Stan_snd++;					 
				 Tim_rq =1000; 	 		
				 return; 	
				}  						
								
			 if(Snd_st & Rd_fw)	
			  {  
				 Init_RdFmw();
				 Snd_rq |= Rd_fw;			
				 Stan_snd++;					 
				 Tim_rq =1000; 	 		
				 return; 	
				}  						
										
			 if((Snd_IndWmb != IndM_wmb) && (FLAGS0 & Okn_sndW))
			  {	
			   if(Init_DtWmb())
				  { 
				   Stan_snd++;	 			   
				   Snd_rq |= Wmb_dtc;
				   Tim_rq =1000; 	 		
				   return; 	
				  }		
				 else
				  {
				   Snd_IndWmb = (Snd_IndWmb + 1) & 0x03FF;
				  }		
			  }	 				
		  }		 
		 break;		
		
/******************************************************************************/	  			
			
		 case 0x01:
			 
		 if(Snd_rq & Stat_dtc)					// odpowiedz na status DC
			{
			 if(FLAGS1 & Rx_srv)
			  {		
				 FLAGS1 &= ~Rx_srv;				
			   if((BufIdlGETmp[4] == 0x00) && (BufIdlGETmp[5] == 0x08) && (BufIdlGETmp[6] == 0x01))
			    {						
					 if(BufIdlGETmp[7] == 0)	
					  {													
						 Ind_fw =0;
					   Snd_st &= ~Rd_fw;	
						 Snd_st &= ~Rd_crc;	 								
					  } 		
					 else
					  {
						 if((Snd_st & Rd_fw) == 0)
						  { 
						   Snd_st |= Rd_crc;									
						  }		
					  }								
					 if(BufIdlGETmp[8] != 0)
					  {  						
						 Snd_st |=  Knf_dct;						 	
						}	
					 if(BufIdlGETmp[9] != 0)	
					  {
						 if((Snd_st & White_L) == 0)
						  { 							
		           Snd_st |= White_L;	
						   Ind_wht =0;							 	
						  }		
						}												
				   Snd_rq &= ~Stat_dtc;
					 Snd_st &= ~Stat_dtc;	
					 Tim_rstGE =0;	
		       Stan_snd =0;	
			     return;						
				  }	
				}
			 else
			  {			
				 if(Tim_rq == 0)
				  {		
					 Snd_rq &= ~Stat_dtc;
					 Snd_st &= ~Stat_dtc;
		       Stan_snd =0;	
			     return;							
				  }	
				}	
			  return;	
			} 	
		
/******************************************************************************/	  			
						
		 if(Snd_rq & Knf_dct)	     // konfiguracja DC
			{  
			 if(FLAGS1 & Rx_srv)
			  {		
				 FLAGS1 &= ~Rx_srv;	
				 if((BufIdlGETmp[4] == 0x00) && (BufIdlGETmp[5] == 0x71) && (BufIdlGETmp[6] == 0x02))
			    {
					 Wr_konfig();							
				   Snd_rq &= ~Knf_dct;
					 Snd_st &= ~Knf_dct;	
		       Stan_snd =0;	
			     return;						
				  }	
				}
			 else
			  {			
				 if(Tim_rq == 0)
				  {		
					 Snd_rq &= ~Knf_dct;	
					 Snd_st &= ~Knf_dct;							
		       Stan_snd =0;	
			     return;							
				  }	
				}	
			  return;	
			} 		
									
/******************************************************************************/	  						

		if(Snd_rq & White_L)	   // whitelist
		 {  
			if(FLAGS1 & Rx_srv)
			 {		
				FLAGS1 &= ~Rx_srv;	
				if((BufIdlGETmp[4] == 0x02) && (BufIdlGETmp[5] == 0x08) && (BufIdlGETmp[6] == 0x03)) 
				 {	
					Wr_whitel();							 
					if(Ind_wht < 7)
					 {		
					  Ind_wht++;
						Snd_rq &= ~White_L; 
						Stan_snd =0;		 
		 	      return; 		   
					 }	 
					else
					 {		
						Ind_wht =0; 
					  Snd_rq &= ~White_L;
						Snd_st &= ~White_L; 
						Stan_snd =0;		 
						return; 		  
					 }	 
				 }
		   }	
			else
			 {
				if(Tim_rq == 0)
				 {					 
			    Snd_rq &= ~White_L;
			    Snd_st &= ~White_L;
		      Stan_snd =0;				  
					return;   
				 }	 
			 }
			return;  
		 }		 
			
/******************************************************************************/
		 
		if(Snd_rq & Rd_crc)	   // crc firmware
		 {  
			if(FLAGS1 & Rx_srv)
			 {		
				FLAGS1 &= ~Rx_srv;	
				if((BufIdlGETmp[4] == 0x00) && (BufIdlGETmp[5] == 0x09) && (BufIdlGETmp[6] == 0x04)) 
				 {	
					Buf_i2c[0] = (uint8_t) (CRC_Fw >> 8); 			 // 4B CRC Fw 	       
					Buf_i2c[1] = (uint8_t) (CRC_Fw);   	 
					Buf_i2c[2] =  BufIdlGETmp[7];
					Buf_i2c[3] = 	BufIdlGETmp[8];
					Buf_i2c[4] =  BufIdlGETmp[9];
					Buf_i2c[5] =  BufIdlGETmp[10];		 
					Wr_i2c(Buf_i2c, 4);  
					 
					Snd_rq &= ~Rd_crc;
					Snd_st &= ~Rd_crc;  					 					 
					Ind_fw =0;		 
					Snd_st |= Rd_fw; 
					Stan_snd =0;				  
					return;    
				 }	 					
		   }	
			else
			 {
				if(Tim_rq == 0)
				 {					 
			    Snd_rq &= ~Rd_crc;			    
		      Stan_snd =0;				  
					return;   
				 }	 
			 }
			return;  
		 }		  
	  		 
/******************************************************************************/		 
							   
 	  if(Snd_rq & Rd_fw)	
		 {  
			if(FLAGS1 & Rx_srv)
			 {		
				FLAGS1 &= ~Rx_srv;	
				if((BufIdlGETmp[4] == 0x02) && (BufIdlGETmp[5] == 0x06) && (BufIdlGETmp[6] == 0x05)) 
				 {	
					Wr_firmware();							
					if(Ind_fw < 223)
					 {		
					  Ind_fw++;
						Snd_rq &= ~Rd_fw; 
						Stan_snd =0;		 
		 	      return; 		   
					 }	 
					else
					 {												
						Buf_i2c[0] = (uint8_t) (ID_Fw >> 8); 			 // zapis 16B znacznika pobranego nowego fw
						Buf_i2c[1] = (uint8_t) (ID_Fw);   	 
						uint8_t i=0; 
						for(i=0; i<16; i++)
						 {	
						  Buf_i2c[2+i] = i;
						 }	 						
						Wr_i2c(Buf_i2c, 16); 
						Wait_ms(30);						 
						HAL_NVIC_SystemReset();	  								 // restart DCTL i przejscie do bootloadera						
					 }	 
				 }
		   }	
			else
			 {
				if(Tim_rq == 0)
				 {					 
			    Snd_rq &= ~White_L;
			    Snd_st &= ~White_L;
		      Stan_snd =0;				  
					return;   
				 }	 
			 }
			return;  
		 }		  
		 		 
/******************************************************************************/	  			
		 
		if(Snd_rq & Wmb_dtc)   // przesylanie ramek wmb
		 {
			if(FLAGS1 & Rx_srv)
			 {		
				FLAGS1 &= ~Rx_srv;				
			  if((BufIdlGETmp[4] == 0x00) && (BufIdlGETmp[5] == 0x05) && (BufIdlGETmp[6] == 0x00))
			   {  
				  Snd_rq &= ~Wmb_dtc;
					Snd_IndWmb = (Snd_IndWmb + 1) & 0x03FF; 		 
			    if(Snd_IndWmb == IndM_wmb)
			     {						 
			      Tim_snd =0;
			      FLAGS0 &= ~Okn_sndW; 
			     }	 			 		
		      Stan_snd =0;	
			    return;						
				 }	
			 }
		  else
			 {			
				if(Tim_rq == 0)
				 {		
					Snd_rq &= ~Wmb_dtc;
		      Stan_snd =0;	
			    return;							
				 }	
			 }	
			return;  
		 } 	
		break; 
				 		 
/******************************************************************************/		 		 
	 }
}


/*******************************************************************************
 Pytanie o konfiguracje dct
*******************************************************************************/

void Init_DtKnf(void)
 {
	uint32_t crc=0;
	uint8_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	Buf_gprs[4] = 0;	     
	Buf_gprs[5] = 20;	    
		 
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x02; 
	 
	crc = calc_crc(Buf_gprs, 22);
	 
	Buf_gprs[22] = (uint8_t) (crc >> 24); 
	Buf_gprs[23] = (uint8_t) (crc >> 16);  
	Buf_gprs[24] = (uint8_t) (crc >> 8);  
	Buf_gprs[25] = (uint8_t) (crc);  
	Il_gprs = 26; 	 
 }	  		 
	 
/*******************************************************************************
 Pytanie o rekord whiteList
*******************************************************************************/

void Init_DtWht(void)
 {
	uint32_t crc=0;
	uint8_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	Buf_gprs[4] = 0;	     
	Buf_gprs[5] = 21;	    
		 
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x03; 
	 
	Buf_gprs[22] = Ind_wht & 0x07;    
	 
	crc = calc_crc(Buf_gprs, 23);
	 
	Buf_gprs[23] = (uint8_t) (crc >> 24); 
	Buf_gprs[24] = (uint8_t) (crc >> 16);  
	Buf_gprs[25] = (uint8_t) (crc >> 8);  
	Buf_gprs[26] = (uint8_t) (crc);  
	Il_gprs = 27; 	 	
 }	

/*******************************************************************************
 Pytanie o crc firmware
*******************************************************************************/ 
 
void Init_RdCrcFw(void)
 {
  uint32_t crc=0;
	uint8_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	Buf_gprs[4] = 0;	     
	Buf_gprs[5] = 20;	    
		 
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x04; 
	  
	crc = calc_crc(Buf_gprs, 22);
	 
	Buf_gprs[22] = (uint8_t) (crc >> 24); 
	Buf_gprs[23] = (uint8_t) (crc >> 16);  
	Buf_gprs[24] = (uint8_t) (crc >> 8);  
	Buf_gprs[25] = (uint8_t) (crc);  
	Il_gprs = 26; 	 	
 }	
	 
/*******************************************************************************
 Pytanie o rekord firmware
*******************************************************************************/

void Init_RdFmw(void)
 {	
	uint32_t crc=0;
	uint8_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	Buf_gprs[4] = 0;	     
	Buf_gprs[5] = 21;	    
		 
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x05; 
	 
	Buf_gprs[22] = Ind_fw;
	 
	crc = calc_crc(Buf_gprs, 23);
	 
	Buf_gprs[23] = (uint8_t) (crc >> 24); 
	Buf_gprs[24] = (uint8_t) (crc >> 16);  
	Buf_gprs[25] = (uint8_t) (crc >> 8);  
	Buf_gprs[26] = (uint8_t) (crc);  
	Il_gprs = 27; 	 	
 }	

/*******************************************************************************
 Dane statusu dct
*******************************************************************************/

void Init_DtCnt(void)
 {
  uint32_t crc=0;
	uint8_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	Buf_gprs[4] = 0;	     
	Buf_gprs[5] = 26;	    
		 
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x01;
	Buf_gprs[22] = Wer_mj;
	Buf_gprs[23] = Wer_mi; 
	Buf_gprs[24] = Wer_rel;
		 
	if(HAL_GPIO_ReadPin(AC_GPIO_Port, AC_Pin) != 0)
	 {				
		Buf_gprs[25] = 0x01;
	 }
	else
	 {			
		Buf_gprs[25] = 0x00; 
	 }	 
				 
	if(FLAGS0 & Tmp_st)
	 {				
		Buf_gprs[26] = 0x01;
	 }
	else
	 {			
		Buf_gprs[26] = 0x00; 
	 }	  
	 
	Buf_gprs[27] = Rssi_gsm;
	 
	crc = calc_crc(Buf_gprs, 28); 		 
	 
	Buf_gprs[28] = (uint8_t) (crc >> 24); 
	Buf_gprs[29] = (uint8_t) (crc >> 16);  
	Buf_gprs[30] = (uint8_t) (crc >> 8);  
	Buf_gprs[31] = (uint8_t) (crc);  
	Il_gprs = 32; 	 
 }	  		

/*******************************************************************************
 Wysylanie ramki WMB do serwera
*******************************************************************************/ 
 
uint8_t Init_DtWmb(void)
 {
  uint32_t crc=0;
	uint16_t i=0; 
	 
	Buf_gprs[0] = 'D';	  
	Buf_gprs[1] = 'T';	   
	Buf_gprs[2] = 'C';	   
	Buf_gprs[3] = 'L';	   
	 
	Rd_Mspi((uint32_t)(Snd_IndWmb << 8), Buf_i2c, 256);
	if((Buf_i2c[1] < 8) || (Buf_i2c[2] != 0x44))
	 {	
	  return 0;
	 }	 
	 	 
	i = Buf_i2c[1]+ 22;
	 
	Buf_gprs[4] = (uint8_t) (i >> 8);	     
	Buf_gprs[5] = (uint8_t) i;	    
	
	for(i=0; i<15; i++)
	 {	
		Buf_gprs[6+i] = Buf_imei[i+1];
	 }	 	 
  Buf_gprs[21] = 0x00;
	 
	for(i=0; i<(Buf_i2c[1] + 2); i++)
   { 	 
		Buf_gprs[22+i] = Buf_i2c[i];
	 }	 
	 
  crc = calc_crc(Buf_gprs, (Buf_i2c[1] + 24));
	 
	Buf_gprs[22 + i++] = (uint8_t) (crc >> 24); 
	Buf_gprs[22 + i++] = (uint8_t) (crc >> 16);  
	Buf_gprs[22 + i++] = (uint8_t) (crc >> 8);  
	Buf_gprs[22 + i++] = (uint8_t) (crc);  
	Il_gprs = Buf_i2c[1] + 28; 	

	for(i=0; i<256; i++)																// wykasowanie wyslanej ramki
   {	 
    Buf_i2c[i] =0;	 
	 }	   
	Wr_Mspi((uint32_t)(Snd_IndWmb << 8), Buf_i2c, 256);		 
	return 1; 
 }	
 
/*******************************************************************************
 Obliczanie CRC
*******************************************************************************/ 
 
 uint32_t calc_crc(uint8_t *Buf, uint16_t Size)
  {		
   uint8_t i,b =0;		
   uint32_t crc =0, crc_tmp =0;
  
   while(Size--)
    {    
     b = *Buf++;    
     for (i = 0; i < 8; ++i)
      {
       crc_tmp = crc;
       crc <<= 1;
       if(b & 0x80)
        {			
         crc |= 1 ;
        }	

       if(crc_tmp & 0x80000000)
        { 	
         crc ^= 0x4C11DB7;
        }	
       b <<= 1;
     }
   }
  return crc;	
}	
	
/*******************************************************************************
 Obsluga bufora nadawczego UART - GE910
*******************************************************************************/

void Obs_txUart(void)
 {
	if(USART1->ISR & USART_ISR_TXE)
   {
		if(U_TxCnt != 0) 
		 {
		  USART1->TDR = BufTxGsm[U_Txpnt++];
			U_TxCnt--; 
			if(U_TxCnt == 0)
			 {	
			  U_Txpnt =0;
			 }	 			 
		 }							
	 }		 
 }		 

/*******************************************************************************
 Obsluga zadan 25ms
*******************************************************************************/ 
 
void Obs_tim25ms(void)
 {
	uint32_t Val_Tsnd = T_snd;
	uint32_t Val_Trx  = T_rx; 
	  
  if(Tim_csq != 0)
	 {	
	  Tim_csq--;
	 }	 	 
	
  if(Tim_rq != 0)
	 {	
	  Tim_rq--;
	 }	 	 
	 	 
	if(FLAGS0 & Trb_rst) 
	 {	
		Val_Tsnd = 1200;
	  Val_Trx  = 1200;  
	 }	 
	
	if(FLAGS0 & Okn_sndW)
	 {	
	  if(Tim_snd < Val_Tsnd)			
		 {	
			Tim_snd++;
		 }	 
		else
		 {			
			Tim_snd =0;
			FLAGS0 &= ~Okn_sndW;			 			 
		 }		
	 } 
	else
	 {		
	  if(Tim_snd < Val_Trx)
		 {	
			Tim_snd++;
		 }	 
		else
		 {			
			Tim_snd =0;
			FLAGS0 |= Okn_sndW;			 			
		 }			 
	 }	 
 }		 
		 
/*******************************************************************************
 Funkcja restartujaca GE-910 w przypadku problemu z komunikacja serwerowa
*******************************************************************************/ 
 
void Obs_rstGE(void)
 {	
  if(Tim_rstGE < TrstGE)		
	 {		
	  Tim_rstGE++;
	 }	 
	else
	 {		
		Tim_rstGE =0; 		
		Tim_ge =200; 		
		Stan_ge =0;	   
	 }
 }	 

/*******************************************************************************
 Filtracja TMP
*******************************************************************************/ 
 
void Obs_tmp(void)
 {
  if(HAL_GPIO_ReadPin(SAB_GPIO_Port, SAB_Pin))
	 {				
		if(FLAGS0 & Tmp_st)
		 {	
			Flt_tmp =0;
		 }	 
    else	
		 {		
		  if(Flt_tmp < 4)
			 {	
			  Flt_tmp++;
			 }	 
		  else
			 {	
				Flt_tmp =0;
				FLAGS0 |= Tmp_st;
				Snd_st |= Stat_dtc; 
				Cnt_stat =0;
				FLAGS0 |= Okn_sndW;
				Tim_snd =0; 				 
			 }		 
		 }
	 }
  else 
	 {	
    if((FLAGS0 & Tmp_st) == 0)
		 {	
			Flt_tmp =0;
		 }	 
    else	
		 {		
		  if(Flt_tmp < 20)
			 {	
			  Flt_tmp++;
			 }	 
		  else
			 {	
				Flt_tmp =0;
				FLAGS0 &= ~Tmp_st;
				Snd_st |= Stat_dtc; 
				Cnt_stat =0; 
				FLAGS0 |= Okn_sndW;
				Tim_snd =0; 				 
			 }		 
		 }
	 }
 }

/*******************************************************************************
 Obsluga diody LD2
*******************************************************************************/
 
void Obs_ld2(void)
 {	
  if(Cnt_100ms < 3)
	 {	
	  Cnt_100ms++;
	 } 		
  else
	 {	
	  Cnt_100ms =0;
		Rol_jld2 >>= 1;
		if(Rol_jld2 == 0)
		 {		
			Rol_jld2 =0x80000000;
		 }		
		if(Mask_ld2 & Rol_jld2)
		 {		
		  HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); 
		 }
		else
		 {		
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);  
		 }	 
   }		 
 }
 
/*******************************************************************************
Zapis konfiguracji DC
*******************************************************************************/ 

void Wr_konfig(void)
 {
	uint8_t i,j=0; 
		
	Buf_i2c[0] = (uint8_t) (IP_prim >> 8); 			 // test nowych ustawien
	Buf_i2c[1] = (uint8_t) (IP_prim);		
  Rd_i2c(Buf_i2c, 108);

  for(i=0; i<108; i++)
   {	  	 
		if(Buf_i2c[i] != BufIdlGETmp[7+i])
		 {		
			j= 255;
		 }	 
	 }	 
	 
	if(j == 255)
	 {			 
	  Buf_i2c[0] = (uint8_t) (IP_prim >> 8); 			 // 4B IP primary 	       
	  Buf_i2c[1] = (uint8_t) (IP_prim);   	 
    Buf_i2c[2] =  BufIdlGETmp[7];
	  Buf_i2c[3] = 	BufIdlGETmp[8];
    Buf_i2c[4] =  BufIdlGETmp[9];
	  Buf_i2c[5] =  BufIdlGETmp[10];		 
	  Wr_i2c(Buf_i2c, 4);
		 
    Buf_i2c[0] = (uint8_t) (IP_sec >> 8); 			 // 4B IP secondary 	       
	  Buf_i2c[1] = (uint8_t) (IP_sec);   	 
    Buf_i2c[2] =  BufIdlGETmp[11];
	  Buf_i2c[3] = 	BufIdlGETmp[12];
	  Buf_i2c[4] =  BufIdlGETmp[13];
	  Buf_i2c[5] =  BufIdlGETmp[14];		 
	  Wr_i2c(Buf_i2c, 4); 
		 
	  Buf_i2c[0] = (uint8_t) (Port_prim >> 8); 		 // 2B Port primary
	  Buf_i2c[1] = (uint8_t) (Port_prim);   	 
	  Buf_i2c[2] = BufIdlGETmp[15];
	  Buf_i2c[3] = BufIdlGETmp[16];
	  Wr_i2c(Buf_i2c, 2);  
		 
	  Buf_i2c[0] = (uint8_t) (Port_sec >> 8); 		 // 2B Port secondary
	  Buf_i2c[1] = (uint8_t) (Port_sec);   	 
	  Buf_i2c[2] = BufIdlGETmp[17];
	  Buf_i2c[3] = BufIdlGETmp[18];
	  Wr_i2c(Buf_i2c, 2);   
		
	  Buf_i2c[0] = (uint8_t) (Apn_N >> 8); 	     	 // 32B APN
	  Buf_i2c[1] = (uint8_t) (Apn_N);   	  		
    for(i=0; i<32; i++)
	   {	
		  Buf_i2c[2+i] = BufIdlGETmp[19+i]; 
	   } 
	  Wr_i2c(Buf_i2c, 32);
	 		
	  Buf_i2c[0] = (uint8_t) (User_N >> 8); 	     // 32B User_N
	  Buf_i2c[1] = (uint8_t) (User_N);   	  		
    for(i=0; i<32; i++)
	   {	
		  Buf_i2c[2+i] = BufIdlGETmp[51+i];
	   } 
    Wr_i2c(Buf_i2c, 32); 
		 
	  Buf_i2c[0] = (uint8_t) (Pass_N >> 8); 	     // 32B Password_N
	  Buf_i2c[1] = (uint8_t) (Pass_N);   	  		
    for(i=0; i<32; i++)
	   {	
	    Buf_i2c[2+i] = BufIdlGETmp[83+i];
	   } 
	  Wr_i2c(Buf_i2c, 32);
	 } 	 
 }
	 
/*******************************************************************************
Zapis whitelist
*******************************************************************************/ 
 
void Wr_whitel(void)
 {
	uint16_t i=0; 
	uint16_t j=0;  
	uint32_t k=0;   
	  
	Buf_i2c[0] = (uint8_t) ((MF_cod + (2 * Ind_wht)) >> 8); 			 // 2B Manufacturer code
	Buf_i2c[1] = (uint8_t) ( MF_cod + (2 * Ind_wht));   	 
  Buf_i2c[2] =  BufIdlGETmp[7];	 
	#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off) 
	Tab_sn[Ind_wht * 2] = BufIdlGETmp[7]; 	 
	#endif 
	Buf_i2c[3] = 	BufIdlGETmp[8];  	 
	#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off) 
	Tab_sn[(Ind_wht * 2) + 1] = BufIdlGETmp[8]; 	  	 
	#endif 
	Wr_i2c(Buf_i2c, 2); 
	 
	for(j=0; j<16; j++) 																					 // Wihitelist	
	 { 
		#ifndef DEBUG	
		HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		#endif				 
		 
		k = (uint32_t) (WHT_ls + (512 * Ind_wht) + (32 * j));
		 
		for(i=0; i<32; i++)
	   {	
		  Buf_i2c[i] = BufIdlGETmp[10+(32*j)+i]; 			
			#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off) 
			Tab_id[(512 * Ind_wht) + (32 * j) + i] = Buf_i2c[i];  
			#endif 
	   }   		 
		Wr_i2cL(k, Buf_i2c, 32); 
	 } 
 }		 
 		 
/*******************************************************************************
Zapis bloku fw
*******************************************************************************/ 
 
void Wr_firmware(void)
 {
	uint16_t i=0; 
	uint16_t j=0;  
	uint32_t k=0;   
	 	 
	for(j=0; j<16; j++) 
	 { 
		#ifndef DEBUG	
		HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		#endif				 
		 
		k = (uint32_t) ((Ind_fw << 9) + FW_Buf + (32*j));
		 
		for(i=0; i<32; i++)
	   {	
		  Buf_i2c[i] = BufIdlGETmp[8+(32*j)+i]; 
	   }   		 
		Wr_i2cL(k, Buf_i2c, 32); 		
	 }	    			
 }	
 
/*******************************************************************************
Filtracja ramki odebranego licznika
*******************************************************************************/ 
 
#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off) 
 
uint8_t Test_whlt(void)
 {
  uint16_t i=0;
	uint8_t  b=0; 
	 
	b = 0; 
	for(i=0; i<8; i++)							// przeszukanie listy producentow
	 {	
	  if((Tab_sn[i * 2] == WMB_Idbuf[4]) && (Tab_sn[(i * 2) + 1] == WMB_Idbuf[3]))
		 {		
			b = 1;
			break;
		 }			
	 }	
	 
	if(b == 0)
	 {	
	  return 0;
	 }	 
	 
	b = 0;  
	for(i=0; i<1024; i++)						// przeszukanie listy licznikow
	 {	
	  if((Tab_id[i * 4] == WMB_Idbuf[8]) && (Tab_id[(i * 4) + 1] == WMB_Idbuf[7]) && (Tab_id[(i * 4) + 2] == WMB_Idbuf[6]) && (Tab_id[(i * 4) + 3] == WMB_Idbuf[5]))
		 {			
			b = 1;
			break;
		 }			
	 }
  return b;
 }	
 
#endif 

/*******************************************************************************
Odczyt whitelist z pamieci przy starcie DC
*******************************************************************************/
 
#if !defined(Test_gsm) && !defined(Test_wmb) && !defined(Wht_lst_off) 
 
void Rd_whtl(void)
 {
	uint16_t i=0; 
	uint16_t j=0;  
	uint16_t k=0;   
  
  Buf_i2c[0] = (uint8_t) (MF_cod >> 8); 			 // odczyt kodow producenta
	Buf_i2c[1] = (uint8_t) (MF_cod);		
  Rd_i2c(Buf_i2c, 16);
	 
	for(i=0; i<16; i++)
	 {	
	  Tab_sn[i] = Buf_i2c[i];
	 }	 
	 
	for(j=0; j<128; j++) 																					 // Wihitelist	
	 { 
		#ifndef DEBUG	
		HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		#endif				 
		 
		k = (uint32_t) (WHT_ls + (32 * j));
		Rd_i2cL(k, Buf_i2c, 32);  
		 
		for(i=0; i<32; i++)
	   {
			Tab_id[(32 * j) + i] = Buf_i2c[i];  		  
	   }   		 		
	 }  
 }	

#endif


/*******************************************************************************
Sprawdzenie czy tryb testowy DC
*******************************************************************************/ 
 
void Test_wrk(void)
 {
  uint8_t i=0;
	 
	for(i=0; i<8; i++)
   {
		if((Tab_sn[2 * i] == 0xFF) && (Tab_sn[(2 * i) + 1] == 0xFF))
		 {	
			FLAGS0 |= Trb_rst;
		  return;		
		 }	 
	 }			 
	FLAGS0 &= ~Trb_rst;  	 
 }	
 
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

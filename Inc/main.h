/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LED3_Pin GPIO_PIN_13
#define LED3_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_14
#define LED2_GPIO_Port GPIOC
#define SDN_Pin GPIO_PIN_15
#define SDN_GPIO_Port GPIOC
#define OSCI_Pin GPIO_PIN_0
#define OSCI_GPIO_Port GPIOF
#define OSCO_Pin GPIO_PIN_1
#define OSCO_GPIO_Port GPIOF
#define CLK_S2_Pin GPIO_PIN_0
#define CLK_S2_GPIO_Port GPIOA
#define CLK_S2_EXTI_IRQn EXTI0_1_IRQn
#define DATA_S2_Pin GPIO_PIN_1
#define DATA_S2_GPIO_Port GPIOA
#define GPIO1_Pin GPIO_PIN_2
#define GPIO1_GPIO_Port GPIOA
#define GPIO0_Pin GPIO_PIN_3
#define GPIO0_GPIO_Port GPIOA
#define CSN_Pin GPIO_PIN_4
#define CSN_GPIO_Port GPIOA
#define SCK_Pin GPIO_PIN_5
#define SCK_GPIO_Port GPIOA
#define MISO_Pin GPIO_PIN_6
#define MISO_GPIO_Port GPIOA
#define MOSI_Pin GPIO_PIN_7
#define MOSI_GPIO_Port GPIOA
#define EN_Pin GPIO_PIN_5
#define EN_GPIO_Port GPIOB
#define MCS_Pin GPIO_PIN_12
#define MCS_GPIO_Port GPIOB
#define MSCK_Pin GPIO_PIN_13
#define MSCK_GPIO_Port GPIOB
#define MMISO_Pin GPIO_PIN_14
#define MMISO_GPIO_Port GPIOB
#define MMOSI_Pin GPIO_PIN_15
#define MMOSI_GPIO_Port GPIOB
#define USB_DIS_Pin GPIO_PIN_9
#define USB_DIS_GPIO_Port GPIOA
#define RES_CE_Pin GPIO_PIN_8
#define RES_CE_GPIO_Port GPIOA
#define SWD_Pin GPIO_PIN_13
#define SWD_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define MON_Pin GPIO_PIN_15
#define MON_GPIO_Port GPIOA
#define AC_Pin GPIO_PIN_3
#define AC_GPIO_Port GPIOB
#define SAB_Pin GPIO_PIN_5
#define SAB_GPIO_Port GPIOB
#define SCL_Pin GPIO_PIN_8
#define SCL_GPIO_Port GPIOB
#define SDA_Pin GPIO_PIN_9
#define SDA_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
/******************************************************************************/

//#define Test_gsm
//#define Test_wmb
//#define Set_ini
//#define DEBUG
//#define Wht_lst_off

#define Tim_stat  120000       // okres wysylania statusu 50 min

#define Tim_set   72000				 // okres wysylania pytania o ustawienia	
#define T_csq     400					 // okres pytania o csq 
#define TrstGE 		432000			 // czas do restartu GE910, gdy brakuje komunikacji z serwerem 

#define T_snd     24000				 // 10 min
#define T_rx      120000 			 // 50 min

/******************************************************************************/

#define Wer_mj    0x01
#define Wer_mi    0x00
#define Wer_rel   0x11

/******************************************************************************/
//FLAGS0
#define mn25ms	  0x01
#define Dt_wmb		0x02
#define Rx_ge			0x04
#define Tmp_st		0x08
#define Trb_rst		0x10	
#define Ok_gpr		0x20	
#define Rx_znTX		0x40
#define Okn_sndW	0x80

//FLAGS1
#define Rx_srv 	  0x01




//Snd_st
#define Stat_dtc	0x0001
#define Knf_dct		0x0002
#define White_L		0x0004
#define Wmb_dtc		0x0008
#define Rd_fw     0x0010
#define Rd_crc    0x0020


/******************************************************************************
Definicje ustawien Data collector
******************************************************************************/

#define ID_set 	  0x0000				// 2B znacznik zainicjowanych ustawien 0x0512

#define IP_prim	  0x0002				// 4B IP primary 

#define IP_sec	  0x0006				// 4B IP secondary

#define Port_prim	0x000A				// 2B Port primary

#define Port_sec	0x000C				// 2B Port secondary

#define Apn_N	 	  0x000E				// 32 x APN 

#define User_N	  0x002E				// 32 x User

#define Pass_N	  0x004E				// 32 x Password

/*****************************************************************************/

#define WHT_ls    0x02000				// 4096B Whitelist

/*****************************************************************************/

#define MF_cod    0x03000				// 16B Manufacturer code

/*****************************************************************************/

#define CRC_Fw    0x03FEC				// 4 bajty crc32 pobranego firmware 

/*****************************************************************************/

#define ID_Fw     0x03FF0				// ciag 16 znakow 0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F
																// ustawiany gdy dostepny jett zbuforowany fw dla bootloadera
																// po wykonaniu aktua;lizacji bootloader powinien skasowac ten ciag
																
/*****************************************************************************/

#define FW_Buf	  0x04000				// bufor firmware DC 0x04000 - 0x1FFFF

/*****************************************************************************/


#define ID_sval	  		0x0512

#define IP_prim_val3  195
#define IP_prim_val2  191
#define IP_prim_val1  180
#define IP_prim_val0  182

#define IP_sec_val3   0
#define IP_sec_val2   0
#define IP_sec_val1   0
#define IP_sec_val0   0

#define Port_prim_val 6000 

#define Port_sec_val  0 

/*****************************************************************************/


/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

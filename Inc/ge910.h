#ifndef _ge910_h_
#define _ge910_h_
#include "stm32f0xx_hal.h"

/*******************************************************************************
 Prototypy funkcji
*******************************************************************************/

void GE910_FN(void);
void Rx_ge910(uint8_t Data);		
void Send_gsm(const uint8_t tablica[]);
uint8_t Test_dtgsm(const uint8_t tablica[]);
extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
void Wr_i2c(uint8_t *Buf, uint8_t Ilosc);
void Wr_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc);
void Rd_i2c(uint8_t *Buf, uint8_t Ilosc);
void Rd_i2cL(uint32_t Adress, uint8_t *Buf, uint8_t Ilosc);
uint16_t Write_bfg(const uint8_t tablica[]);	
void Rd_AtOk(void);			
void ByteToAsc(uint16_t Value);
uint8_t Write_AscBf(uint8_t Indeks);
void Obs_csq(void);
void Rx_stSrv(void);
uint8_t Knw_bin(uint8_t Data);

/*******************************************************************************
 Tablice komend at gsm
*******************************************************************************/
			
const uint8_t Tab_at[]      = {'A','T',13,10,0};			
const uint8_t Tab_ate0[]    = {'A','T','E','0',13,10,0};
const uint8_t Tab_cme[]		  = {'A','T','+','C','M','E','E','=','1',13,10,0};
const uint8_t Tab_csg[]		  = {'A','T','+','C','G','S','N',13,10,0};
const uint8_t Tab_simd[]		= {'A','T','#','S','I','M','D','E','T','=','1',13,10,0};	
const uint8_t Tab_csq[]		  = {'A','T','+','C','S','Q',13,10,0};
const uint8_t Tab_RxCSQ[]   = {'+','C','S','Q',':',0};
const uint8_t Tab_cgd[]     = {'A','T','+','C','G','D','C','O','N','T','=','1',',','"','I','P','"',',','"',0};
const uint8_t Tab_usr[]		  = {'A','T','#','U','S','E','R','I','D','=','"',0};	
const uint8_t Tab_pas[]		  = {'A','T','#','P','A','S','S','W','=','"',0};	
const uint8_t Tab_act[]		  = {'A','T','#','S','G','A','C','T','=','1',',','1',13,10,0};
const uint8_t Tab_err[]		  = {'E','R','R','O','R',0};		
const uint8_t Tab_sga[]	    = {'#','S','G','A','C','T',':',0};
const uint8_t Tab_ss[]		  = {'A','T','#','S','S','=','1',13,10,0};
const uint8_t Tab_sh[]	    = {'A','T','#','S','H','=','1',13,10,0};		
const uint8_t Tab_NoCr[]    = {'N','O',' ','C','A','R','R','I','E','R',0};
const uint8_t Tab_sts[]	    = {'#','S','S',':',0};
const uint8_t Tab_Con[]     = {'A','T','#','S','D','=','1',',','0',',',0};
const uint8_t Tab_SendE[]   = {'A','T','#','S','S','E','N','D','E','X','T','=','1',',',0};
const uint8_t Tab_SetTCP[]  = {'A','T','#','S','C','F','G','E','X','T','=','1',',','2',',','1',',','2','4','0',',','0',',','1',13,10,0};	
const uint8_t Tab_SetCfg2[] = {'A','T','#','S','C','F','G','E','X','T','2','=','1',',','0',',','0',',','1',',','0',',','0',13,10,0};
const uint8_t Tab_srg[]     = {'S','R','I','N','G',':',' ','1',0};
const uint8_t Tab_cpin[]		= {'A','T','+','C','P','I','N','?',13,10,0};
const uint8_t Tab_ready[]   = {'+','C','P','I','N',':',' ','R','E','A','D','Y',0}; 
const uint8_t Tab_simpin[]	= {'+','C','P','I','N',':',' ','S','I','M',' ','P','I','N',0};
const uint8_t Tab_pin5664[] = {'A','T','+','C','P','I','N','=','"','5','6','6','4','"',13,10,0};




		
#endif


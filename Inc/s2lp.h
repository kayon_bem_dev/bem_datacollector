#ifndef _s2lp2_h_
#define _s2lp2_h_
#include "stm32f0xx_hal.h"

/*******************************************************************************
 Prototypy funkcji
*******************************************************************************/

void S2LP_INIT(void);
void S2LP_RX(void);
uint8_t Read_Spr(uint8_t Adr_spr);
void Write_spi(uint8_t *Spi_bf, uint8_t Size);
void Test_s2lp(void);
extern void Wait_ms(uint16_t Tim_ms);

/*******************************************************************************
 Polecenia S2LP
*******************************************************************************/

#define Sp_Cmd      0x80            // nagl�wek polecen
#define Sp_Rd       0x01            // nagl�wek odczytu	 
#define Sp_Wr       0x00            // nagl�wek zapisu
#define Sp_Tx       0x60            // polecenie startu nadawania RF			
#define Sp_Rx       0x61            // polecenie startu odbioru RF				
#define Sp_Rdy      0x62            // polecenie przejscia do stanu READY	
#define Sp_Stb      0x63            // polecenie przejscia do stanu STANDBY	
#define Sp_Rst      0x70            // restet

/*******************************************************************************
 Czestotliwosc pracy S2LP
*******************************************************************************/

#define  F868_950		(uint32_t)  0x6216B6FE    // 868.950 Mhz
   
/******************************************************************************/

#endif
